import React from 'react';
import './style.css';
function ListItem({ itemText, index, selectItem, isSelected }) {
  const itemStyle = isSelected ? 'listItem' : null;

  return (
    <li className={itemStyle} onClick={() => selectItem(index)}>
      {itemText}
    </li>
  );
}

export default ListItem;
