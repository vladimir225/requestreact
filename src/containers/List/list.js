import React from 'react';
import ListItem from './list.item';

class List extends React.Component {
  state = {
    selectedItem: null
  };

  selectItem = index => {
    this.setState({
      selectedItem: index
    });
  };

  render() {
    const { list, loading } = this.props;
    const { selectedItem } = this.state;
    const listItems = list.map((el, index) => (
      <ListItem key={el} itemText={el} index={index} isSelected={selectedItem === index} selectItem={this.selectItem} />
    ));
    const isList = loading ? 'List loading' : listItems;
    return <ul>{isList}</ul>;
  }
}

export default List;
